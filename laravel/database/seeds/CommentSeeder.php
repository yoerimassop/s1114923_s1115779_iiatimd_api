<?php

use Illuminate\Database\Seeder;
use Carbon\carbon;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->insert([
            'user_id' => '2',
            'post_id' => '1',
            'comment' => 'Moeilijk recept, wel lekker!',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('comments')->insert([
            'user_id' => '3',
            'post_id' => '1',
            'comment' => 'Niet te beschrijven, heerlijk gerecht. Dit ga ik zeker vaker koken.',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('comments')->insert([
            'user_id' => '2',
            'post_id' => '2',
            'comment' => 'Te veel ingrediënten helaas...',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('comments')->insert([
            'user_id' => '2',
            'post_id' => '3',
            'comment' => 'mmmmmm smullen',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('comments')->insert([
            'user_id' => '2',
            'post_id' => '4',
            'comment' => 'Heerlijk gegeten, een echte aanrader!',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('comments')->insert([
            'user_id' => '1',
            'post_id' => '5',
            'comment' => 'Heerlijk gegeten, een echte aanrader!',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('comments')->insert([
            'user_id' => '1',
            'post_id' => '6',
            'comment' => 'Heerlijk gegeten, een echte aanrader!',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('comments')->insert([
            'user_id' => '1',
            'post_id' => '7',
            'comment' => 'Heerlijk gegeten, een echte aanrader!',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('comments')->insert([
            'user_id' => '1',
            'post_id' => '8',
            'comment' => 'Heerlijk gegeten, een echte aanrader!',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')

        ]);
        DB::table('comments')->insert([
            'user_id' => '1',
            'post_id' => '9',
            'comment' => 'Heerlijk gegeten, een echte aanrader!',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('comments')->insert([
            'user_id' => '1',
            'post_id' => '10',
            'comment' => 'Heerlijk gegeten, een echte aanrader!',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('comments')->insert([
            'user_id' => '1',
            'post_id' => '11',
            'comment' => 'Heerlijk gegeten, een echte aanrader!',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);


    }
}
