<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Yoeri',
            'lastname' => 'Massop',
            'photo' => 'avatar1.jpg',
            'email' => 'yoerimassop@gmail.com',
            'device_token' => 'skjdhksfhkjsfhdksdjf',
            'password' =>  bcrypt(12341234)
        ]);

        DB::table('users')->insert([
            'name' => 'Julian',
            'lastname' => 'Visschedijk',
            'photo' => 'avatar2.jpg',
            'email' => 'julianvisschedijk@gmail.com',
            'device_token' => 'skjdhksfhkjsfhdksdjf',
            'password' =>  bcrypt(12341234)
        ]);

        DB::table('users')->insert([
            'name' => 'Anne',
            'lastname' => 'van Velden',
            'photo' => 'avatar3.jpg',
            'email' => 'anna@gmail.com',
            'device_token' => 'skjdhksfhkjsfhdksdjf',
            'password' =>  bcrypt(12341234)
        ]);
    }
}
