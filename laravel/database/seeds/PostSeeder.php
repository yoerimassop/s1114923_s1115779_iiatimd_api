<?php

use Illuminate\Database\Seeder;
use Carbon\carbon;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            'user_id' => 1,
            'recipe_title' => 'Tempeh met satésaus',
            'photo' => 'sate.webp',
            'recipe_description' => 'Begin met het snijden van de tempeh in blokjes. Meng vervolgens in een schaal 4 el ketjap manis, 0,5 tl sambal, de olijfolie en het sap van een halve limoen. Doe dan de tempeh blokjes erbij en laat dit, afgedekt met wat vershoudfolie, 45-60 minuten marineren. Ondertussen heb je nu mooi de tijd om bijvoorbeeld iets van nasi te maken. Begin vlak voordat je de tempeh gaat bakken met het maken van de satesaus (volgens dit satesaus recept). Hiervoor heb je nodig, pindakaas, basterdsuiker, water/kokosmelk, 2 el ketjap manis, 1 tl sambal, sap van een halve limoen. Rijg de tempeh blokjes aan een prikker en bak ze volgens circa 5 minuten in een pan met een beetje olie. Dan is het alleen nog een kwestie van op een lekker bord nasi leggen en wat zelfgemaakte satesaus erover. Eet smakelijk!',
            'side_description' => 'Overheerlijke saté gemaakt op de Indonesische traditionele manier. Houd je van aziatisch eten, dan is dit het perfecte gerecht!',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('posts')->insert([
            'user_id' => 1,
            'recipe_title' => 'Spaghetti met spekjes',
            'photo' => 'spaghetti.webp',
            'recipe_description' => 'Kook de pasta volgens de instructies op het pak. Snipper de ui en snijd ook de bosuien in stukjes. Doe de spekjes in een keukenpan (zonder olie of boter) en bak ze bruin en krokant. Daarna kunnen de ui, knoflook en tuinerwten erbij. Na een minuut of 5 voeg je de kruidenroomkaas en de bosuien toe. Laat de kruidenroomkaas zachtjes smelten en voeg vervolgens de gekookte pasta toe. Breng het geheel op smaak met een snufje zout en peper. Meng alles door elkaar. Serveer met wat geraspte Parmezaanse kaas en fijngesneden peterselie.',
            'side_description' => 'Geen zin om lang in de keuken te staan, maat wel zin in een lekker pastagerecht? Dan kunnen we deze spaghetti met spekjes zeker aanbevelen!',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('posts')->insert([
            'user_id' => 1,
            'recipe_title' => 'Souvlaki',
            'photo' => 'souvlaki.webp',
            'recipe_description' => 'Wij maken de souvlaki met kipfilet maar zoals we hierboven al zeiden: je kunt ook kiezen voor varkensvlees of lamsvlees. Snijd het vlees in blokjes. Pers de tenen knoflook uit en doe ze samen met de olijfolie, oregano, tijm en citroensap in een vershoudbakje. Voeg vervolgens het vlees toe en een snufje zout en peper en meng alles goed door elkaar. Laat het vlees nu zo lang mogelijk marineren (bijvoorbeeld een nachtje in de koelkast). Korter kan uiteraard ook, maar de smaak trekt dan niet zo goed in het vlees. Prik het vlees aan metalen spiesen als je ze op de barbecue bakt of aan sateprikkers als je ze in een grillpan bakt. Laat de sateprikkers eerst wel even weken in een bakje water. Bak de souvlaki circa 5-10 minuten totdat het vlees gaar is. Souvlaki wordt eigenlijk altijd op een barbecue gebakken, maar het kan bijvoorbeeld ook in een grillpan. Uiteraard is de smaak dan wel iets anders.',
            'side_description' => 'Souvlaki is een spiesje met gemarineerd vlees. Meestal is dit varkensvlees maar ook kipfilet en lamsvlees wordt hiervoor gebruikt.',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('posts')->insert([
            'user_id' => 1,
            'recipe_title' => 'Rendang ajam (met kip en rijst)',
            'photo' => 'rendang.webp',
            'recipe_description' => 'Schil de gember en snijd vervolgens in kleine stukjes. Snipper de ui, snijd de teentjes knoflook fijn en snijd de sereh in stukken. Doe vervolgens de gember, ui, knoflook en sereh samen met de rode peper, koriander, komijn en een scheutje olijfolie in een keukenmachine. Hak alles tot er een soort kruidenpasta overblijft. Snijd vervolgens de kipfilet in stukjes en breng op smaak met een klein beetje zout en peper. Doe een klein klontje boter in een braadpan en bak de kruidenpasta voor een minuut of 2-3. Doe dan de kip erbij en bak deze rondom bruin. Dan kan de kokosmelk en de ketjap erbij. Zet een deksel schuin op de pan en laat de rendang ajam zachtjes pruttelen, ongeveer 30-45 minuten totdat de ‘saus’ is ingedikt. Kook ondertussen nog lekker wat rijst en groente. Eet smakelijk!',
            'side_description' => 'Zie hier, rendang ajam oftewel rendang met kip. Dit lekkere stoofvlees recept eten wij graag met rijst en eventueel wat gewokte groente.',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('posts')->insert([
            'user_id' => 2,
            'recipe_title' => "Zoete aardappel nacho's",
            'photo' => 'nachos.webp',
            'recipe_description' => 'Verwarm de oven voor op 200 graden. Snijd de zoete aardappels in frietjes of gebruik een kant-en-klare zoete aardappelfrietjes. Meng de zoete aardappelfrietjes met een beetje (olijf)olie en wat Mexicaanse kruiden. Verdeel de frietjes over een bakplaat met bakpapier en bak ongeveer 25 minuten in de oven. Daarna haal je de frietjes uit de oven en verdeel je de cheddar er overheen. Zet de bakplaat nog een paar minuten onder de grill totdat de kaas gesmolten is.
Ondertussen snipper je de rode ui en snijd je de tomaten in blokjes. Meng in een bakje de rode ui en tomatenblokjes door elkaar. Voeg een scheutje olie en een snufje zout en peper toe. Snijd de avocado doormidden, verwijder de pit en snijd in blokjes. Serveer de zoete aardappel nachos met zure room, avocado, tomaat en rode ui.',
            'side_description' => 'Nachos met gehakt staan bij ons regelmatig op tafel. Lekker met bijvoorbeeld wat rijst en guacamole erbij. We hebben een lekkere variatie op dit recept gemaakt, namelijk zoete aardappel nachos met tomaat en zure room. Erg lekker (al zeg ik het zelf).',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('posts')->insert([
            'user_id' => 2,
            'recipe_title' => "Rode currysoep",
            'photo' => 'curry.webp',
            'recipe_description' => 'Snipper de ui en snijd de knoflook fijn. Snijd ook de paprika, tomaten en wortels in stukjes. Giet een scheutje olie in een pan en bak de ui en paprika. Voeg na een paar minuten de knoflook, tomaten, wortel en rode curry pasta toe. Meng alles door elkaar en voeg na 2 minuten de kokosmelk en bouillon toe. Breng de soep aan de kook en kook de soep ongeveer 10-15 minuten totdat de wortel zacht is. Vervolgens pureer je de soep met een staafmixer. Serveer wat verse bosui bovenop de soep en met wat (naan)brood erbij. Eet smakelijk!',
            'side_description' => 'Een lekker tussendoortje of eeen gerecht om me te starten, uniek maar oh zo lekker!',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('posts')->insert([
            'user_id' => 2,
            'recipe_title' => "Tiktok pasta kruidenroomkaas",
            'photo' => 'pasta.webp',
            'recipe_description' => 'Verwarm als eerste de oven voor op 200 graden. Doe de cherrytomaatjes in een ovenschaal en besprenkel met wat olijfolie, zout en peper. Leg in het midden van de ovenschaal de kruidenroomkaas. Ook hierover een beetje olijfolie, zout en peper. Zet de ovenschaal nu voor 25 minuten in de oven. Kook ondertussen de pasta volgens de instructies op de verpakking. Snijd ondertussen een teentje knoflook fijn evenals wat blaadjes basilicum. Haal na 25 minuten de ovenschaal uit de oven en roer de tomaatjes en de kruidenroomkaas door elkaar, samen met de knoflook en verse basilicum. Meng dan de pasta erdoor en het is tijd om op te scheppen.',
            'side_description' => 'De TikTok pasta, of de gebakken feta pasta zoals wij hem zelf noemen, hebben we een tijdje geleden geprobeerd nadat we het zo ontzettend vaak waren tegengekomen.',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('posts')->insert([
            'user_id' => 2,
            'recipe_title' => "Vega burger",
            'photo' => 'burger.webp',
            'recipe_description' => 'Bak de hamburgerbroodjes af volgens de instructies op het pak. Snijd de tomaat in dunne plakjes en was de sla. Snijd eventueel de sla wat kleiner. Giet een scheutje olie in een pan en bak de vega burgers volgens de bereidingswijze op het pak. Snijd de broodjes horizontaal door midden en besmeer ze met wat zelfgemaakte gembersaus. Hak de walnoten in stukjes. Beleg de broodjes met sla, een burger, tomaat, walnoten en wat nachochips. Broodje erop en klaar!',
            'side_description' => 'Deze vega burger met nachochips, sla en walnoten is in ongeveer 20 minuten klaar en een echte aanrader! Nog wat Mexicaanse zoete aardappelfrietjes erbij en het feestmaal is compleet.',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('posts')->insert([
            'user_id' => 3,
            'recipe_title' => "Tomatensoep",
            'photo' => 'tomaten.webp',
            'recipe_description' => 'Snipper het sjalotje en snijd de teentjes knoflook fijn. Giet een scheutje olijfolie in een hapjespan en fruit de ui aan voor ongeveer 5 minuten. Bak de laatste minuut de knoflook en kruiden mee. Snijd ondertussen de tomaten in stukken. Bak de tomaten nog 5 minuten mee en voeg dan de bouillon toe. Laat de soep 10 minuten zachtjes pruttelen. Pureer de soep vervolgens met een staafmixer en breng op smaak met een snufje zout en peper. Een scheutje kookroom erdoor en klaar. Serveer de soep met een broodje en wat smeersels.',
            'side_description' => 'Een zeldzaam recept voor tomatensoep met een romige na smaak!',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('posts')->insert([
            'user_id' => 3,
            'recipe_title' => "Spinazie Ricotta quiche",
            'photo' => 'spinazie.webp',
            'recipe_description' => 'Verwarm de oven voor op 200 graden en haal de plakjes bladerdeeg alvast uit de vriezer. Snipper de ui, pel de teentjes knoflook en snijd ze fijn. Giet een klein scheutje olie in een hapjespan en fruit de ui samen met de knoflook aan, voor ongeveer 3-4 minuten. Dan doe je beetje voor beetje de spinazie erbij om te laten slinken. klop ondertussen in een kom de ricotta, kookroom en eieren los, samen met een snufje zout en peper. Vet een ovenschaal of springvorm in en bekleed deze vervolgens met de plakjes bladerdeeg. Prik dan nog wat gaatjes in het deeg met een vork. Meng de spinazie, ui en knoflook door het eimengsel, wat geraspte kaas er nog door en dan doe je de inhoud van de kom over de bladerdeeg. Zet de spinazie ricotta quiche voor ongeveer 30-35 minuten on de oven.',
            'side_description' => 'Deze spinazie ricotta quiche is het resultaat, erg lekker. Kopje soep erbij of een salade en je hebt een hele lekkere simpele maaltijd.',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('posts')->insert([
            'user_id' => 3,
            'recipe_title' => "Italiaanse broodje bal",
            'photo' => 'broodjebal.webp',
            'recipe_description' => 'Bak de Italiaanse broodjes af volgens de aanwijzingen op de verpakking. Snipper de ui en het sjalotje en snijd een teentje knoflook fijn. Breng het gehakt op smaak met de Italiaanse kruiden en een snufje zout en peper. Meng dan het sjalotje, een eitje en wat paneermeel erdoor. Maak van het gehakt 4 gehaktballen en bak ze vervolgens in een pan met een klontje boter gaar in ongeveer 10 minuten. Fruit ondertussen in een steelpannetje met een beetje olijfolie de ui aan samen met het teentje knoflook. Voeg na vijf minuten de tomato frito toe, samen met een eetlepel balsamico azijn en een snufje peper en zout. Laat de saus zachtjes pruttelen. Doe de ballen in de tomatensaus en dan verdeel je alles over de broodjes. Nog wat geraspte mozzarella en verse basilicum erover en aan tafel!',
            'side_description' => 'Een lekkere Italiaanse bol met gehaktballetjes in tomatensaus met mozzarella. Eventueel kun je er nog een kopje soep bij serveren.',
            'created_at' =>  Carbon::now()->format('Y-m-d H:i:s')
        ]);


    }
}
