<?php

namespace App;

use App\User;
use App\Post;


use Illuminate\Database\Eloquent\Model;
use DateTimeInterface;


class Comment extends Model
{
    protected $casts = [
        'created_at' => 'datetime:d-m-Y H:i',
    ];

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id', 'id');
    }
}
