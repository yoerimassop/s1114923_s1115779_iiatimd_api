<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Like;
use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PostsController extends Controller
{
    public function create(Request $request){
        $post = new Post;
        $post->user_id = Auth::user()->id;
        $post->recipe_title = $request->title;
        $post->side_description = $request->side_description;
        $post->recipe_description = $request->description;

        if ($request->photo != ""){
            $photo = time().'.jpg';
            file_put_contents('storage/posts/'.$photo, base64_decode($request->photo));
            $post->photo = $photo;
        }

        $post->save();
        $post->user;
        return response()->json([
            'success' => true,
            'message' => 'posted',
            'post' => $post
        ]);
    }

    public function update(Request $request){
        $post = Post::find($request->id);
        if(Auth::user()->id != $post->user_id) {
            return response()->json([
                'success' => false,
                'message' => 'unauthorized access'
            ]);
        }
        $post->recipe_title = $request->title;
        $post->side_description = $request->side_description;
        $post->recipe_description = $request->description;

        if ($request->photo != ""){
            $photo = time().'.jpg';
            file_put_contents('storage/posts/'.$photo, base64_decode($request->photo));
            $post->photo = $photo;
        }

        $post->update();
        return response()->json([
            'success' => true,
            'message' => 'post edited',
            'post' => $post
        ]);
    }

    public function delete(Request $request){
        $post = Post::find($request->id);
        if(Auth::user()->id != $post->user_id) {
            return response()->json([
                'success' => false,
                'message' => 'unauthorized access'
            ]);
        }
        if ($post->photo != ""){
            Storage::delete('public/posts/'.$post->photo);
        }
        $post->delete();
        return response()->json([
            'success' => true,
            'message' => 'post deleted'
        ]);
    }

    public function index(){
        $posts = Post::orderBy('id','desc')->get();
        foreach ($posts as $post){
            $post->user;
            $post['commentsCount'] = count($post->comments);
            $post['likesCount'] = count($post->likes);
            $post['selfLike'] = false;
            foreach ($post->likes as $like){
                if($like->user_id == Auth::user()->id){
                    $post['selfLike'] = true;
                }
            }
        }
        return response()->json([
            'success' => true,
            'posts' => $posts
        ]);
    }

    public function myPosts(){
        $posts = Post::where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();
        $total_likes = 0;
        $total_comments = 0;
        foreach ($posts as $post) {
            foreach ($post->likes as $like) {
                $total_likes ++;
            }
            foreach ($post->comments as $comment) {
                $total_comments ++;
            }
        }
        $user = Auth::user();
        return response()->json([
            'success' => true,
            'likes' => $total_likes,
            'comments' => $total_comments,
            'posts' => $posts,
            'user' => $user
        ]);
    }

    public function sendLikeNotification($user_id)
    {
        $device_token = User::where('id', '=' , $user_id)->first()->device_token;
        return $this->sendNotification($device_token, array(
            "title" => "Post like",
            "body" => "Je recept is geliked!"
        ));
    }

    public function sendCommentNotification($user_id)
    {
        $device_token = User::where('id', '=' , $user_id)->first()->device_token;
        return $this->sendNotification($device_token, array(
            "title" => "Post reactie",
            "body" => "Er is een reactie op de post van een recept!"
        ));
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function sendNotification($device_token, $message)
    {
        $SERVER_API_KEY = 'AAAA01DJK5k:APA91bHfh3SMsjAh__qxoAL3ya3CZ97ikwvJ3FNm43ym7E6slZrh08R2RohCS0XTrAl9CTX5-BarV2N3nNeiDhhl5OmtlzHI4kE4NFe8uUJKo16EDqIKDamasa8utZ5sbzDCneE2ClS5';
        $data = [
            "to" => $device_token, // for single device id
            "data" => $message
        ];
        $dataString = json_encode($data);

        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }

}
