<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Like;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $creds = $request->only(['email', 'password']);
        if(!$token=auth()->attempt($creds)){
            return response()->json([
                "success" => false,
                "message" => "invalid credentials"
            ]);
        }

        if($request->device_token){
            $user = User::find(Auth::user()->id);
            $user->device_token = $request->device_token;
            $user->update();
        }


        return response()->json([
            "success" => true,
            "token" => $token,
            "user" => Auth::user()
        ]);
    }

    public function register(Request $request)
    {
        $encryptedPass = Hash::make($request->password);

        $user = new User;
        try {
            $user->email = $request->email;
            $user->password = $encryptedPass;
            $user->device_token = $request->device_token;
            $user->save();
            return $this->login($request);
        }catch (Exception $e){
            return response()->json([
                "success" => false,
                "message" => '' . $e
            ]);
        }
    }

    public function logout(Request $request)
    {
        try {
            $user = User::find(Auth::user()->id);
            $user->device_token = "";
            $user->update();
            JWTAuth::invalidate(JWTAuth::parseToken($request->token));
            return response()->json([
                "success" => true,
                "message" => "logout succes"
            ]);
        }catch(Exception $e){
            return response()->json([
                "success" => false,
                "message" => '' . $e
            ]);
        }
    }

    public function saveUserInfo(Request $request)
    {
        $user = User::find(Auth::user()->id);
        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $photo= $user->photo;
        if ($request->photo!=''){
            //hierdoor is er geen duplicatie in de namen
            $photo = time(). '.jpg';
            file_put_contents('storage/profiles/' . $photo, base64_decode($request->photo));
            $user->photo = $photo;
        }

        $user->update();
        return response()->json([
            'success' => true,
            'photo' => $photo
        ]);
    }

}
