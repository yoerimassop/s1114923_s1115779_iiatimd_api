<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('login', 'Api\AuthController@login');
Route::post('register', 'Api\AuthController@register');



Route::middleware(['jwtAuth'])->group(function () {
    Route::get('logout', 'Api\AuthController@logout');
    Route::post('save_user_info', 'Api\AuthController@saveUserInfo');
    Route::post('posts/create', 'Api\PostsController@create');
    Route::post('posts/delete', 'Api\PostsController@delete');
    Route::post('posts/update', 'Api\PostsController@update');
    Route::get('posts', 'Api\PostsController@index');
    Route::get('posts/my_posts', 'Api\PostsController@myPosts');

    Route::post('comments/create', 'Api\CommentsController@create');
    Route::post('comments/delete', 'Api\CommentsController@delete');
    Route::post('posts/comments', 'Api\CommentsController@index');

    Route::post('posts/like', 'Api\LikesController@like');

    Route::post('send-notification-like/{user_id}', 'Api\PostsController@sendLikeNotification');
    Route::post('send-notification-comment/{user_id}', 'Api\PostsController@sendCommentNotification');
});


